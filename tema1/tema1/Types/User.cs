﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tema1.Types
{
    public class User
    {
        public string Name;
        public string Username;
        public string Password;
        public string Role;

        public User()
        {

        }

        public User(string name, string usr, string pass, string role)
        {
            Name = name;
            Username = usr;
            Password = pass;
            Role = role;
        }
    }
}
