﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tema1.Types
{
    class Report
    {
        public DateTime StartDate;
        public DateTime EndDate;
        public List<Appointment> apps;

        public Report()
        {
            apps = new List<Appointment>();
        }

        public Report(DateTime sd, DateTime ed, List<Appointment> ap)
        {
            StartDate = sd;
            EndDate = ed;
            apps = ap;
        }

    }
}
