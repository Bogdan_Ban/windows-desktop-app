﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tema1.Types
{
    class Appointment
    {
        public string Date;
        public string ClientName;
        public string Phone;
        public string Car;
        public string Problem;
        public int status;

        public Appointment()
        {

        }

        public Appointment(string dt, string cn, string ph, string car, string pb, int s)
        {
            Date = dt;
            ClientName = cn;
            Phone = ph;
            Car = car;
            Problem = pb;
            status = s;
        }
    }
}
