﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using tema1.BL;
using tema1.DAL;

namespace tema1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            textBox2.PasswordChar = '*';
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            UserService ser = new UserService();
            bool logged = ser.login(textBox1.Text, textBox2.Text);
            Console.WriteLine(ser.rolee);
            if (ser.rolee.Equals("ServiceAgent"))
            {
                Form3 frm3 = new Form3();
                frm3.Show();
            }
            else
                if(ser.rolee.Equals("Admin"))
                {
                    Form2 frm2 = new Form2();
                    frm2.Show();
                }
                else
                {
                    Console.WriteLine("Error!");
                }
        }
    }
}
