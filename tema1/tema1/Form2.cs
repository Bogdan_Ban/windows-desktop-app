﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using tema1.Types;
using tema1.BL;

namespace tema1
{
    public partial class Form2 : Form
    {
        UserService usrSer = new UserService();
        ReportsManager rep = new ReportsManager();

        public Form2()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //working
            User u = new User(textBox1.Text,textBox2.Text,textBox3.Text,"ServiceAgent");
            usrSer.regAgent(u);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //NOT working
            rep.searchApps(textBox4.Text, textBox5.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // depends on searchApps()
            rep.printReport();
        }
    }
}
