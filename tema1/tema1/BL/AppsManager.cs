﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tema1.DAL;
using tema1.Types;

namespace tema1.BL
{
    class AppsManager
    {
        private List<Appointment> apps;

        public void createApp(Appointment a)
        {
            AppDAL.getInstance().addApp(a);
        }

        public void modifyAppStatus(Appointment a)
        {
            AppDAL.getInstance().editApp(a);
        }

        public void printApps()
        {
            apps = AppDAL.getInstance().getAllApps();
            foreach(Appointment ap in apps)
            {
                Console.WriteLine("Data: " + ap.Date + " & Clientul: " + ap.ClientName);
            }
        }
    }
}
