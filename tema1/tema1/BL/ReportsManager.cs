﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tema1.DAL;
using tema1.Types;

namespace tema1.BL
{
    class ReportsManager
    {
        private Report report = new Report();

        public void printReport()
        {
            Console.WriteLine("Start date: " + report.StartDate.ToString() + "\n");
            Console.WriteLine("End date: " + report.EndDate.ToString() + "\n");
            Console.WriteLine("Appointments:\n");
            foreach(Appointment a in report.apps)
            {
                Console.WriteLine("Date: " + a.Date + ", Client name: " + a.ClientName);
            }
        }

        public void searchApps(string d1, string d2)
        {
            report.apps = AppDAL.getInstance().getApps(d1,d2);  
        }
    }
}
