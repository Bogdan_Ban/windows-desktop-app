﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using tema1.DAL;
using tema1.Types;

namespace tema1.BL
{
    public class UserService
    {
        UsersDAL usersDAL;
        public string rolee = "";

        public UserService()
        {

        }

        public UserService(UsersDAL usDal)
        {
            usersDAL = usDal;
        }

        public bool login(string usr, string pass)
        {
            User us = UsersDAL.getInstance().getUser(usr, pass);

            if (us == null)
            {
                return false;
            }
            else
            {
                if (us.Role.Equals("ServiceAgent"))
                {
                    rolee = "ServiceAgent";
                }
                else
                    if (us.Role.Equals("Admin"))
                    {
                        rolee = "Admin";
                    }
            }
            return true;

        }

        public string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
{
                sBuilder.Append(data[i].ToString("x2"));
            }
            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public void regAgent(User us)
        {
            string encryptedPass = getMd5Hash(us.Password);
            UsersDAL.getInstance().registerAgent(us.Name, us.Username, encryptedPass);
        }
    }
}
