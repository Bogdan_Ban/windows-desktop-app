﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using tema1.Types;
using tema1.BL;

namespace tema1
{
    public partial class Form3 : Form
    {
        AppsManager am = new AppsManager();
        public Form3()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //working
            am.printApps();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //working
            Appointment a = new Appointment(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, 0);
            am.createApp(a);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //working
            /*nu sunt necesare toate datele pentru a modifca statusul*/
            //doar dateTime
            Appointment a = new Appointment(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, 0);
            am.modifyAppStatus(a);
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
