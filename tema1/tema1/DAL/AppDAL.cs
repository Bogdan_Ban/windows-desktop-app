﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using  tema1.Types;


namespace tema1.DAL
{
    class AppDAL
    {
        private static AppDAL _appsDAL = null;
        private String _connectionString = @"Data Source=DESKTOP-M9EGDI7\SQLEXPRESS;Initial Catalog=tema1;Trusted_Connection=Yes;";
        SqlConnection _conn = null;
        private AppDAL()
        {
            try
            {
                _conn = new SqlConnection(_connectionString);
            }
            catch (SqlException e)
            {
                _conn = null;
            }
        }

        public static AppDAL getInstance()
        {
            if (_appsDAL == null)
            {
                _appsDAL = new AppDAL();
            }
            return _appsDAL;
        }

        public List<Appointment> getAllApps()
        {
            List<Appointment> list = new List<Appointment>();
            string sqlQuery = "SELECT * FROM tema1.dbo.Appointment;";

            SqlCommand command = new SqlCommand(sqlQuery, _conn);

            try
            {
                _conn.Open();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Appointment a = new Appointment(reader["Datee"].ToString(), reader["ClientName"].ToString(),
                    reader["Phone"].ToString(), reader["Car"].ToString(), reader["Problem"].ToString(), Int32.Parse(reader["Status"].ToString()));
                    list.Add(a);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                //If an exception occurs, write it to the console
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                _conn.Close();
            }
            return list;
        }

        public List<Appointment> getApps(string datetime1, string datetime2)
        {
            Appointment a = null;
            List<Appointment> list = new List<Appointment>();
            String sql = " SELECT* FROM tema1.dbo.Appointment WHERE Datee >= '" + datetime1 + "' and Datee <= '" + datetime2+"';";
            Console.WriteLine(sql);
            SqlCommand cmd = new SqlCommand(sql, _conn);
            try
            {
                //_conn.Close();
                _conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    a = new Appointment(reader["Datee"].ToString(), reader["ClientName"].ToString(),
                    reader["Phone"].ToString(), reader["Car"].ToString(), reader["Problem"].ToString(), Int32.Parse(reader["Status"].ToString()));
                    list.Add(a);
                }
                reader.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                _conn.Close();
            }
            return list;
        }

        public void addApp(Appointment app)
        {
            String sql = " INSERT INTO Appointment (Datee,ClientName,Phone,Car,Problem,Status) VALUES ('" + app.Date + "', '" +
                app.ClientName + "', '" + app.Phone + "', '" + app.Car + "', '" + app.Problem + "', '0');";
            try
            {
                _conn.Open();
                SqlCommand cmd = new SqlCommand(sql, _conn);
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void editApp(Appointment app)
        {
            String sql = " update Appointment set Status = 1 where Datee = '" + app.Date + "' and ClientName = '"
                + app.ClientName + "';";
            try
            {
                _conn.Open();
                SqlCommand cmd = new SqlCommand(sql, _conn);
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
