﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using tema1.Types;


namespace tema1.DAL
{
    public class UsersDAL
    {
        private static UsersDAL _usersDAL = null;
        private String _connectionString = @"Data Source=DESKTOP-M9EGDI7\SQLEXPRESS;Initial Catalog=tema1;Trusted_Connection=Yes;";
        SqlConnection _conn = null;
        private UsersDAL()
        {
            try
            {
                _conn = new SqlConnection(_connectionString);
            }
            catch (SqlException e)
            {
                //de facut ceva error handling, afisat mesaj, etc..
                _conn = null;
            }
        }

        public static UsersDAL getInstance()
        {
            if (_usersDAL == null)
            {
                _usersDAL = new UsersDAL();
            }
            return _usersDAL;
        }

        public User getUser(string username, string password)
        {
            User u = null;
            String sql = " SELECT* FROM Users WHERE Username = '" + username + "' AND Password='" + password + "'";
            Console.WriteLine(sql);
            try
            {
                _conn.Open();
                SqlCommand cmd = new SqlCommand(sql, _conn);
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                u = new User(reader["Name"].ToString(), reader["Username"].ToString(),
                    reader["Password"].ToString(), reader["Role"].ToString());

                _conn.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
            return u;
        }

        public void registerAgent(string name, string usr, string enPass)
        {
            String sql = "INSERT INTO Users (Name, Username, Password, Role) VALUES ('" + name + "', '" + usr + "', '" + enPass +
                "', 'ServiceAgent');";
            try
            {
                _conn.Open();
                SqlCommand cmd = new SqlCommand(sql, _conn);
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
